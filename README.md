# Datree Cicd with Gitlab

- https://cloudgeeks.ca

- https://datree.io

- datree
- https://hub.datree.io/setup/cli-arguments#flags

```resources
- '--no-record' - Don’t send policy checks metadata to the backend  https://hub.datree.io/setup/cli-arguments#flags
- Offline mode - https://hub.datree.io/setup/offline-mode
- Datree data privacy info: https://hub.datree.io/data-privacy
```

##### helm 

```helm
helm template -f hello/values.yaml --namespace datree --create-namespace hello/ --dry-run > datree.yaml
```
##### datree
- datree-installation-linux
```datree-installation-linux
curl https://get.datree.io | /bin/bash
```

```datree
export DATREE_TOKEN=[YOU-TOKEN] # Grab that from webui

datree test datree.yaml -p prod
```

- gitlab runner configuration

- https://www.youtube.com/watch?v=CdWVsIU2QJI&t=303s

- https://gitlab.com/quickbooks2018/gitlab-docker-runner/-/tree/main

##### docker-slim build

- https://www.youtube.com/watch?v=ZhU2_AXUK0U

- https://github.com/quickbooks2018/aws/blob/master/docker-slim


##### Gitlab Runner Installation Script

- Docker & Docker Compose Version 2 Installation

```docker
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh && rm -f get-docker.sh
```

- Gitlab Runner Installation & Registration

- Ubuntu

```gitlab
https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner
```

```gitlab-runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
apt-get install gitlab-runner && systemctl enable gitlab-runner
```

- Gitlab Runner in Docker

- Important Learning Note: To make gitlab runner work with docker executor, gitlab runner should be running in Host machine not in container, so must use shell executor with gitlab docker.

- https://docs.gitlab.com/runner/install/docker.html

- Linux

```
docker run -id --name gitlab-runner --restart unless-stopped --network host -w /mnt -v $(which docker):/usr/bin/docker -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

- Windows

- docker compose version 2

- https://docs.docker.com/compose/install/linux/#install-using-the-repository

```
docker run -id --name gitlab-runner --network host --restart unless-stopped -w /mnt -v "c:/users/Muhammad Asim/Desktop/kubernetes:/mnt" -v "//var/run/docker.sock:/var/run/docker.sock" gitlab/gitlab-runner:latest

docker exec -it gitlab-runner bash

curl -fsSL https://get.docker.com -o get-docker.sh

sh get-docker

rm -f get-docker

cd /home/gitlab-runner

cp -rv /mnt/.kube .
```

- Gitlab Registration

```gitlab-runner-registration
gitlab-runner register

gitlab-runner status

gitlab-runner list

gitlab-runner verify --delete -t uLxMwdxdC1vmbXtceomu -u https://gitlab.com/

gitlab-runner verify

gitlab-runner --help
```

- Docker Socket Permissions

- Create docker-permissions.sh

```socket
#!/bin/bash
# Purpose: Set Docker Socket Permissions after reboot & Docker Logging

###########################
# Docker Socket Permissions
###########################
cat <<EOF > ${HOME}/docker-socket.sh
#!/bin/bash
chmod 666 /var/run/docker.sock
#End
EOF

chmod +x ${HOME}/docker-socket.sh

cat <<EOF > /etc/systemd/system/docker-socket.service
[Unit]
Description=Docker Socket Permissions
After=docker.service
BindsTo=docker.service
ReloadPropagatedFrom=docker.service

[Service]
Type=oneshot
ExecStart=${HOME}/docker-socket.sh
ExecReload=${HOME}/docker-socket.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

systemctl start docker-socket.service

systemctl enable docker-socket.service

################
# Docker Logging
################

cat << EOF > /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "1024m",
    "max-file": "5"
  }
}

EOF

shutdown -r now

# End
```

- Docker-Slim Installation

```docker-slim
curl -sL https://raw.githubusercontent.com/docker-slim/docker-slim/master/scripts/install-dockerslim.sh | sudo -E bash -
```

- Helm Instllation

```helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
rm -f get_helm.sh
```

- Datree Installation

```datree
apt update -y && apt install -y unzip && curl https://get.datree.io | /bin/bash
```

- DATREE_TOKEN (please add this in gitlab variables)

- kubectl Installation

```kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/local/bin/kubectl
```

- kubernetes kind cluster Stable Installation
```kind
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
chmod +x kind
mv ./kind /usr/local/bin
kind create cluster --name cloudgeeks-ca --image kindest/node:v1.21.12
kubectl cluster-info
```

- AWS Access Keys create environment variables in gitlab

```secret
export AWS_ACCESS_KEY_ID=AKIA2LV4KIQL6APEF5ZR
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-east-1
```

- Install AWSCLi

```awscli
cd /root
apt update -y && apt install -y sudo curl
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm -rf *.zip
aws --version
```

- KMS Encryption & Decryption

- url https://www.youtube.com/watch?v=c_RItEvqPuM

- url https://raw.githubusercontent.com/quickbooks2018/aws/master/wordpress-kms-encryption

- url https://github.com/quickbooks2018/aws/blob/master/wordpress-kms-encryption

- KUBECONFIG

```KUBECONIG for gitlab-runner
cd /home/gitlab-runner
cp -rv /root/.kube .
chown -R gitlab-runner:gitlab-runner .kube
```

- Port Forwarding & SSH Tunnerl Port Forward

```kubectl
kubectl port-forward svc/hello --address 0.0.0.0 8080:80 -n datree
```

```ssh-tunnel
ssh -N -L 8080:0.0.0.0:8080 cloud_user@3.134.78.49
```


- Build tags

- url https://hub.docker.com/r/quickbooks2018/datree/tags
